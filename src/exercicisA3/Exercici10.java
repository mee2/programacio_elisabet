package exercicisA3;
import java.util.Scanner;
public class Exercici10 {
/* 
1.10.- Algorisme que llegeix dos n�meros enters positius i diferents i ens diu 
si el major �s m�ltiple del menor, o el que �s al mateix, 
que el menor �s divisor del major. */

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1, num2;
		System.out.println("Introdueix un numero ");
		Scanner reader = new Scanner(System.in);
		num1= reader.nextInt();
		System.out.println("Introdueix un altre numero ");
		num2= reader.nextInt();
		if (num1>num2) {
			if(num1%num2==0) {
				System.out.println(num1+" es multiple de "+num2);
			}else {
				System.out.println("Els numeros no son divisors entre si");	
			}
		}
		else {
			if(num2%num1==0) {
				System.out.println(num2+ " es multiple de "+num1);
			}
		
		else {
			System.out.println("Els numeros no son divisors entre si");	
		}
		}
		reader.close();
	}
}
